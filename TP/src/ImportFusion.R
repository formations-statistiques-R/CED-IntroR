# Exercices Gestion de données
# Importation et fusion
# Les donnees sont dans ...TP/data
setwd("~/formation/cours-stat-R-2018/CED-IntroR/TP/data")

# importation des trois fichiers: etat1.csv, etat2.csv, etat3.csv
etat1 <- read.csv("etat1.csv",sep=";",header = TRUE)
etat2 <- read.csv("etat2.csv",sep=",",header = TRUE)
etat3 <- read.csv("etat3.csv",sep="",row.names=1,header = TRUE)

# Fusion par clef
names(etat1)
names(etat2)
names(etat3)

# variable region commune a etat1 et etat3
etat13 <- merge(etat1,etat3,by="region")
names(etat13)
# variable etat commune a etat2 et etat13
etat123 <- merge(etat2,etat13,by="etat")
names(etat123)
summary(etat123)