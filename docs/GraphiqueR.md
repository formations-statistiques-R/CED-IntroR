
Les graphiques constituent souvent le point de départ d'une analyse statistique. 

Pour visualiser les données avec R, on dispose d'au moins trois packages à savoir **graphics** pour réaliser les graphiques de base R, **lattice** qui permet, en autre, de représenter des données dans lesquelles la variable en ordonnée dépend de plusieurs variables et enfin **ggplot2** (où *gg* signifie "**G**rammar of **G**raphics") qui permet à travers une grammaire graphique, de concevoir des graphiques couche par couche et qui offre un haut niveau de contrôle sur les composantes graphiques.

On verra dans ce cours, l'étude des graphiques conventionnels (graphics), des références de tutoriaux vous seront fournies pour l'utilisation de **lattice** et **ggplot2**.

Pour commencer il peut être intéressant de regarder quelques exemples de représentations graphiques pouvant être construites avec R, vous utiliserez la fonction **demo** (script: ~/CED-IntroR/TP/exemples/demosGraphics.R):


```R
demo(graphics)  
```

## Les fonctions graphiques de base

Les fonctions couramment utilisées.

* Variables qualitatives <br />
**pie(x)** # diagramme camenbert <br />
**barplot(x)** # diagramme bâton 


* Variables quantitatives <br />
**hist(x,nclass)** # histogramme de x <br />
**boxplot(x)** # boite à moustache <br />
**stripchart**(x) <br />


* Graphiques 3D <br />
**image(x,y,z)** # forme d'image <br />
**persp(x,y,z)** # forme de nappe c <br />
**contour(x,y,z)** # les contours <br />
**Fonction utile** : z=outer(x,y,fonction)


* Tableau et matrices<br />
**pairs(data)** # nuage de points colonne par colonne de data<br />
**matplot(data)** # trace chaque colonne de data

### La fonction plot

La fonction **plot** est une fonction générique de R permettant de représenter tous les types de données.

#### Nuage de points d'une variable y en fonction d'une variable x

Représenter à intervalles réguliers les points de la fonction: $\bf{ x \longrightarrow sin(2\pi x) \quad x \in [0,1[}$


```R
# Construire la grille de points
grillex <- seq(0,1,length=50)
fx <- sin(2*pi*grillex)
plot(x=grillex,y=fx)
```

Il est plus classique d'utiliser la fonction plot avec des formules du type  $\bf{y^{\sim}x}$


```R
plot(fx~grillex)
```

#### A partir d'un data-frame

On reprend le fichier de données **mtcars**

   *Description* : les données ont été extraites du magazine Motor Trend US de 1974 et comprennent la consommation de carburant et 10 aspects de la conception et des performances de 32 automobiles (modèles 1973-1974).

   *Format*:

Un fichier de données "mtcars" avec 32 observations sur 11 variables:

mpg: Miles/(US) gallon (variable quantitative)
cyl: Number of cylinders (variable qualitative)
disp: Displacement (cu.in.) (variable quantitative)
hp:    Gross horsepower (variable quantitative)
drat: Rear axle ratio (variable quantitative)
wt:    Weight (1000 lbs) (variable quantitative)
qsec: 1/4 mile time (variable quantitative)
vs: V/S (variable qualitative)
am:    Transmission (0 = automatic, 1 = manual) (variable qualitative)
gear: Number of forward gears (variable qualitative)
carb: Number of carburetors (variable qualitative)



```R
mtcars <- read.csv("data/mtcars.csv",header=TRUE,sep=",")
str(mtcars)
summary(mtcars)
# Definir en tant que variable qualitative
mtcars$cyl <- factor(mtcars$cyl)
mtcars$vs <- factor(mtcars$vs)
mtcars$am <- factor(mtcars$am)
mtcars$gear <- factor(mtcars$gear)
mtcars$carb <- factor(mtcars$carb)
summary(mtcars)
```

* Représentation de deux variables qualitatives: les variables **drat** et **wt**


```R
plot(mtcars[,"drat"],mtcars[,"wt"])
```

Comme les deux variables sont contenues dans le même *data-frame*, une syntaxe plus simple permet d'avoir directement les noms de variables en labellés d'axe.


```R
plot(drat~wt,data=mtcars)
```

Cette représenation graphique aurait pu être obtenue en définissant de manière explicite les labels des axes à l'appel de la fonction "plot" avec les arguments **xlab** et **ylab**.


```R
plot(mtcars[,"drat"],mtcars[,"wt"],xlab="wt",ylab="drat")
```

* Pour représenter une variable quantitative ("wt") en fonction d'une variable qualitative ("cyl"), on utilisera la même syntaxe:


```R
plot(wt~cyl,data=mtcars,xlab="Number of cylinders",ylab="Weight (1000 lbs)")
```

La fonction plot retourne une boîte à moustaches par modalité de la variable qualitative. Ce graphique permet de voir rapidement si il existe un effet de la variable *Numbers of cylinders* sur la varibale *Weight*. Il peut être obtenu à partir de la fonction **boxplot**.


```R
boxplot(wt~cyl,data=mtcars)
```

* Représenation de deux variables qualitatives par un diagramme bande:


```R
plot(cyl~vs,data=mtcars)
```

Pour chaque modalité du facteur explicatif ("vs") on la fréquence relative de chaque modalité du facteur à expliquer ("cyl") et la largeur de bande est proportionnelle à la fréquence de la modalité du facteur explicatif("vs").

* On peut **représenter une variable qualitative ("vs") en fonction d'une variable quantitative ("drat")**. La variable quantitative est découpée en classes selon la même méthode qu'un histogramme et dans chaque classe sont calculées les fréquences relatives de chaque modalité de la variable qualitative.


```R
plot(cyl~drat,data=mtcars)
```

Ce graphique peut être obtenu directement à partir de la fonction **splineplot**.


```R
spineplot(cyl~drat,data=mtcars)
```


```R
plot(mtcars[,"cyl"]~mtcars[,"drat"],xlab="Rear axle ratio",ylab="Number of cylinders")
```


```R
colorInd <-function(v,seuil)
{
  sapply(v,function(x) {if (x < seuil ) tabcol="black" else tabcol="red"})
}
 colS <- colorInd(mtcars$drat,3.5)
plot(mtcars[,"drat"], ylab="Rear axle ratio",cex=0.5, pch=16,col=colS)
```


```R
plot(mtcars[,"drat"], ylab="Rear axle ratio",cex=0.5, pch=16,col="red")
```

## Ajout à un graphique

Une fois le graphique tracé, on peut le compléter par d'autres informations:

* ajout de *lignes* avec la fonction **lines()**
* ajout de *text* avec la fonction **text()**


```R
x<- 1:10;y <-x^2;z <-x^3;w<-x^(-2)
plot(x,y,type="l",main="Fonctions puissances",cex.main=0.9)
text(4,y[4]+5,expression(x^2))
lines(x,z,col="red")
text(2,z[2]+5,expression(x^3))
lines(x,w,col="blue")
text(6,w[6]+5,expression(x^-2))
```

* ajout de *points* par la fonction **point()**
* ajout de *flèches* par la fonction **arrow()**
* ajout de *segments* par la fonction **segments()**
* ajout de *polygones* par la fonction **polygone()**

## Plusieurs graphiques dans la même fenêtre

On veut faire figurer plusieurs graphiques dans une même fenêtre.

* Pour des graphiques de même taille: on utilise la **fonction par()**.<bf\>
 
 L'instruction **par(mfrow=c(n,p))** organise **n\*p** graphiques en *n lignes et p
colonnes*.


```R
# Les donnees
rm(list=ls())
data(iris)
```


```R
par(mfrow=c(1,2)) # une ligne et deux colonnes
plot(Sepal.Length~Sepal.Width,data=iris,pch=0)
plot(Petal.Length~Petal.Width,data=iris,pch=16)
```

* Pour des graphiques de taille différentes, on utilisera la fonction **layout**.
Elle admet comme argument, une matrice de taille **nrow X ncol**, les valeurs de la
matrice correspondent aux numéros des graphiques qui doivent être dessinés dans
chaque case.
Pour disposer trois graphiques sur deux lignes:

$$mat =\left[\begin{array}{cc}
1&1 \\ 2&3
\end{array}\right]$$

Trois graphiques sur deux lignes:


```R
mat <-matrix(c(1,2,1,3),nrow=2)
layout(mat)
plot(Sepal.Length~Species,data=iris,pch=0)
plot(Sepal.Length~Sepal.Width,data=iris,pch=21)
plot(Petal.Length~Petal.Width,data=iris,pch=16)
```

## Personnalisation des graphiques

Certains paramètres sont modifiables directement dans la commande graphique, d'autres sont accessibles dans la fonction par qui gère tous les paramètres graphiques du device:

* Utiliser des couleurs avec **col="red"**, en utilisant des chiffres ou un code
**"RGB"**.

* de mettre un titre avec l'argument **"main"**
* Contrôler l'aspect des axes et de leur label.

Certains paramètres sont modifiables directement dans l'appel de la fonction graphique, d'autres sont accessibles par la fonction **par()**.


```R
x <- 1:10
y <- x^2
par(fg='blue',bg='#f2a1c2') # couleur en avant plan et en arrière plan
plot(x,y,type="l",axes=FALSE,xlab="x",ylab="y", main=expression(f(x) == x^2),cex.main=0.9)
axis(1,at=c(1,5,10),label=c("coord 1","coord 2","coord 3"), cex.axis=0.8)
axis(2,at=seq(1,100,length=10),cex.axis=0.8)
```


```R
help(par)
```

* On peut ajouter une légende:

On ajoute une légende avec la fonction legend


```R
ecarty <- range(iris[,"Sepal.Length"])
plot(iris[1:75,"Sepal.Length"],type="l")
lines(iris[76:150,"Sepal.Length"],ylim=ecarty,col="red")
legend("topleft",legend=c("sem1","sem2"),col=c("black","red"),lty=1)
```

* On peut insérer des symboles ou des formules mathématiques en utilisant la syntaxe "latex":


```R
plot(1,1,cex.lab=0.8,xlab=expression(bar(x) == sum(frac(x[i],10),i==1,10)))
```


```R
x <- seq(-4, 4, len = 101)
y <- cbind(sin(x), cos(x))
matplot(x, y, type = "l", xaxt = "n",main = expression(paste(plain(sin) * phi, " and ",
 plain(cos) * phi)),
 ylab = expression("sin" * phi, "cos" * phi), # only 1st is + xlab = expression(paste("Phase Angle ", phi)),
 col.main = "blue")
axis(1, at = c(-pi, -pi/2, 0, pi/2, pi),labels = expression(-pi, -pi/2, 0, pi/2, pi))
```

Pour connaître l'ensemble des paramètres qui permettent d'améliorer les
graphiques, voir la fonction **par()** (help(par))

## Représentation de distributions

Pour représenter la distribution d'une variable continue, il existe des solutions classiques déjà programmées.

### Histogramme


```R
par(mfrow=c(1,3))
hist(iris$Sepal.Length,col="skyblue",cex.main=0.8)
hist(iris$Sepal.Length,col="red",main="Histogramme de Sepal.Length",prob=TRUE,xlab="Sepal.Length",cex.main=0.8)
plot(density(iris$Sepal.Length),cex.main=0.8,main="Noyaux",xlab="Sepal.Length")
```

L'histogramme est un estimateur de la densité si on indique **prob=TRUE**

### Barplot


```R
data(iris)
```


```R
Sepal.Length <- cut(iris$Sepal.Length,4)
Sepal.Length
```


```R
# Sepal.Length est de class factor
par(mfrow=c(1,2))
summary(Sepal.Length)
#
barplot(table(Sepal.Length),col=1:nlevels(Sepal.Length),legend=TRUE)
barplot(table(Sepal.Length),col=1:nlevels(Sepal.Length),horiz=TRUE)
```

### pie: diagramme circulaire (camembert)


```R
pie(table(Sepal.Length),col=1:nlevels(Sepal.Length),radius = 1.)
```

### Diagramme en barre par groupe



```R
# On decoupe la variable iris$Sepal.Width en 5 classes
class(iris$Sepal.Width)
Sepal.Width <- cut(iris$Sepal.Width,5)
class(Sepal.Width)
```


```R
par(mfrow=c(1,3))
barplot(table(Sepal.Length,Sepal.Width),col=1:nlevels(Sepal.Length),legend=TRUE)
#
barplot(table(Sepal.Length,Sepal.Width),col=1:nlevels(Sepal.Length),beside=TRUE)
#
barplot(table(Sepal.Length,Sepal.Width),col=1:nlevels(Sepal.Length),beside=TRUE,horiz=TRUE)
```

## Graphiques en plusieurs dimensions

Les fonctions de représentation 3D sur une grille de points sont les fonctions
**persp**(3D avec effet de perspective), **contour** (lignes de niveau) et **image** (lignes
de niveau avec effet de couleur).


```R
f <- function(x,y) 10*sin(sqrt(x^2+y^2))/sqrt(x^2+y^2)
x <-seq(-10,10,length=30) # maillage
y <- x
```

* Evaluons la fonction en chaque point de la grille avec la fonction **outer()**.


```R
z <-outer(x,y,f)
```

* Traçons la fonction 3D avec la fonction **persp()**.


```R
persp(x,y,z,theta=30,phi=30,expand=0.5)
```

Nous pouvons obtenir les **courbes de niveaux** en utilisant la fonction **contour** (lignes de
niveau) ou la fonction **image()** (lignes de niveau avec effet de couleur).


```R
par(mfrow=c(1,2))
contour(x,y,z)
image(x,y,z)
```

Nous pouvons utiliser le package **rgl** pour construire la surface de réponse du
graphique précédent.


```R
library(rgl)
rgl.surface(x,y,z)
plot3d(x,y,z)
```

## Package lattice

### Livres:

* Lattice  Multivariate Data Visualization with R - Deepayan Sarkar (Edition Springer)
    (web: http://lmdvr.r-forge.r-project.org/figures/figures.html)

### Web

* [Introduction to R - Lattice](https://www.isid.ac.in/~deepayan/R-tutorials/labs/04_lattice_lab.pdf)


## Package ggplot2

### Web
* [Introduction to R graphics with ggplot2 - R Tutorials](http://tutorials.iq.harvard.edu/R/Rgraphics/Rgraphics.html)
* [Data Visualization with ggplot2](https://www.datacamp.com/courses/data-visualization-with-ggplot2-1)
* https://fr.slideshare.net/VincentIsoz/tutorial-ggplot2

### Livres

* Elegant Graphics for Data Analysis - Hadley Wickham (Edition Springer)

* R Graphics Cookbook - Winston Chang (Edition O'Reilly)


