
# Introduction
## Qu'est ce que R? 
R est un système d'analyse statistique et graphique créé par Ross Ihaka et Robert Gentleman.
R est à la fois un logiciel et un langage interprété dont la conception dérive du langage S développé au sein des laboratoires AT&T Bell dans les années 80 et disponible actuellement sous la forme du logiciel S-PLUS commercialisé par la compagnie Insightful. Il y a des différences dans la conception de R et celle de S, pour en savoir plus sur ce point, se reporter au [R-FAQ3](https://cran.r-project.org/doc/FAQ/R-FAQ.html).

Le langage R possède aujourd'hui une communauté mature d'utilisateurs et de développeurs qui ont créé et partagé des milliers de package via le _Comprehensive R Archive Network_ (CRAN, https://cran.r-project.org).

R comporte de nombreuses fonctions pour les analyses statistiques, les
graphiques, le traitement et le stockage des données. Les graphiques sont visualisés à l'écran ou sur papier et peuvent être exportés sous divers formats (jpg, png, bmp, ps, pdf, emf, pictex,
xfig); les formats disponibles peuvent dépendre du système d'exploitation.
Les résultats des analyses statistiques sont affichés à l'écran ou peuvent être sauvegardés ou exportés dans un fichier pour être utilisés dans des analyses ultérieures.

R est un langage de programmation simple et efficace qui permet à l'utilisateur d'écrire ses propres algorithmes en utilisant le traitement conditionnel, les boucles, la récursivité, les fonctions intrinsèques et utilisateurs et des fonctions d'entrée /sorties. 

R comprend une suite d'opérateurs po

ur les calculs sur des tableaux, en particulier des matrices.

R offre les possibilités d'améliorer ses performances en convertisant le haut niveau d'interprétation du code R en un langage compilé écrit en C, C++ ou Fortran (**RCPP**,...)
R permet de s'adapter à l'architecture des processeurs multi-coeurs et aux clusters de plusieurs noeuds en fournissant des outils aux développeurs qui lui permet d'utiliser des techniques de programmation parallèle (**parallel**,**multicore**,**snow**,...)

Un dernier point important, les statisticiens ont implémentés dans R sous forme de packages directement intégrées dans R, des centaines de procédures adaptées à une grande variété d'applications.

## Pourquoi R?
### Avantages

R a des avantages majeurs
1. Il est gratuit et le restera puisqu'il est distribué librement sous les termes de la [GNU General Public Licence](http://www.gnu.org/)
2. Il est disponible sur Internet, via un grand réseau de serveurs.
3. Il fonctionne sur de nombreux systèmes d'exploitation Unix et dérivés incluant
Darwin, Mac OS X, Linux, FreeBSD et Solaris; sur 
Microsoft Windows.
4. Il est le produit d'une collaboration internationale entre
statisticiens et informaticiens;
5. Il ne limite pas l'utilisateur à un ensemble de procédures ou d'options, à une méthode presse-bouton tout en utilisant les packages déjà intégrés dans R.
6. Il fournit des outils consistants (bibliothèques, language,...) qui permettent de travailler avec des objets complexes de grande taille. 
7. Il existe une documentation technique complète à laquelle participent les utilisateurs, des tutoriaux adaptés à des usages différents et aussi plusieurs bons livres sur les méthodes statistiques qui utilisent R (ou S) pour l'illustration.
8. Il est entièrement programmable, les procédures répétitives peuvent facilement être automatisées par des scripts écrits par l'utilisateur. Il est facile d'écrire ses propres fonctions, et pas trop compliqué d'écrire des paquets entiers pour implémenter de nouveaux algorithmes.
9. Le code source est publié, ainsi vous avez accès aux algorithmes exacts avec une validation possible les statisticiens experts.
10. Il peut échanger des données en format MS-Excel, texte, format fixe et délimité(CSV,...), de sorte que les jeux de données existants sont facilement importés, et les résultats calculés en R facilement exportables. 
11. La plupart des programmes écrits avec S-PLUS fonctionneront sans modification, ou avec des changements mineurs, dans R.

### Inconvénients
R présentent quelques inconvénients:

1. L'utilisateur doit définir lui-même la séquence des analyses et les exécuter pas à pas, cependant, il est facile de créer des scripts avec toutes les étapes dans les éditeurs des interfaces utilisateurs de R (dans ce cours RStudio).
2. L'utilisateur doit apprendre à penser autrement la gestion de ses données, de penser les objets R comme des classes ce qui lui permettra de bénéficier des avantages des langages objet. Les méthodes opérant sur un objet en fonction de sa classe.
3. L'utilisateur doit apprendre un langage, à la fois pour les commandes mais aussi pour la spécification des analyses statistiques.

### Alternatives
Il y a d'autres logiciels qui permettent de faire des calculs statistiques et du traitement de données, ils peuvent souvent interagir entre eux avec la possibilité de programmer ses propres algorithmes ou d'interagir avec R. Aucun d'entre eux n'est open-source, ce qui signifie que vous devez faire confiance à l'entreprise pour faire les calculs correctemen

   - S-PLUS: : implémentation commerciale du langage S di usée par TIBCO.
   - SAS: un concurrent de S-PLUS, très utilisée dans l'industrie, programmable.
   - [Des programmes statistiques spécifques](http://www.stata.com/links/statistical-software-providers/).
   - Tableurs:  Microsoft Excel (Data Analysis.  .  .  ),

## Installer R
R est distribué librement sous les termes de la [GNU General Public Licence](http://www.gnu.org/)
, son développement et sa distribution sont assurés par plusieurs statisticiens
rassemblés dans le [**R Development Core Team**](https://cran.r-project.org/mirrors.html).

Pour un meilleur suivi du cours, nous vous conseillons d'installer **R** (Version 3.4.x au moins) sur votre ordinateur personnel, il peut être téléchargé sur différents serveurs appelés “Miroirs” et situés dans de nombreux pays. Plusieurs miroirs sont disponibles en France, on y trouvera des versions adaptées à différentes plateformes (Windows, Mac-OS et Linux).

Sous plusieurs formes : 

    - des exécutables précompilés pour Windows, Linux et OSX (Macintosh).
    - de sources(écrit principalement en C et certaines routines en Fortran) qu'il faudra compiler avant de les utiliser.
   - [Installer R sous **Windows**](https://cloud.r-project.org/bin/windows/base/)

   - [Installer R sous **Mac**](https://cloud.r-project.org/bin/macosx/)
   
   - [Sous **Linux**](https://cran.r-project.org/mirrors.html), R n’est fourni que comme un outil en ligne de commande, utilisez votre gestionnaire de packages habituel (de préférence, choisir un miroir proche de chez vous).

Plusieurs packages sont fournis avec l'installation de base, de nouveaux packages peuvent être facilement ajoutés, l'installation peut se faire en local ou pour l'ensemble des utilisateurs. Nous verrons comment installer de nouveaux package dans la suite du cours.

[RStudio](https://www.rstudio.com/products/RStudio/) est un environnement de développement intégré (IDE), qui propose des outils et facilite l’écriture de scripts et l’usage de R au quotidien. C’est une interface bien supérieure à celles fournies par défaut lorsqu’on installe R sous Windows ou sous Mac-OS

[Installer RStudio](https://www.rstudio.com/products/rstudio/download/#download), téléchargez la version adaptée à votre système.


## RStudio : interface conviviale

[RStudio](https://www.rstudio.com/products/RStudio/) est un environnement de développement intégré (IDE), qui 
propose des outils et facilite l’écriture de scripts et l’usage de R au quotidien. C’est une interface bien 
supérieure à celles fournies par défaut lorsqu’on installe R sous Windows ou sous Mac-OS

Des versions libres sont téléchargeables sur la page https://www.rstudio.com/products/rstudio/download/#download

- Téléchargez la version adaptée à votre système et installez la sur votre poste de travail.

- Cet environnement inclut dans une seule interface attractive:  

<img src="../figures/InterfaceRStudio.jpg",width="80%",height="80%">


 - une console (Console), 
 - un terminal (Terminal),
 - un éditeur de code, 
 - sorties graphiques (Plot), 
 - l'historique (History),
 - une aide en ligne (Help), 
 - le contenu de l'espace de travail (Environment),
 - un gestionnaire de fichier (Files),
 - le gestionnaire de paquets (Packages).
 
**Cette interface peut être personnalisée**, voir: [Customizing RStudio](https://support.rstudio.com/hc/en-us/articles/200549016-Customizing-RStudio).
 
On peut utiliser les gestionnaires de version **git**, **svn**,... sous RStudio, ce qu'on ne fera pas dans ce cours, [voir le tutoriel](https://informatique-mia.inra.fr/r4ciam/sites/ciam.inra.fr.r4ciam/files/Tutoriels/UtilisationGit13mai.pdf).
 

### Modification et exécution du code

L'éditeur de source de RStudio inclut une variété de fonctionnalités améliorant la productivité, y compris la mise en évidence de la syntaxe, l'achèvement du code, l'édition de plusieurs fichiers et la recherche/remplacement.

RStudio vous permet également d'exécuter de manière flexible le code R directement depuis l'éditeur de source.  Travailler dans l'éditeur de source facilite la reproduction de séquences de commandes et le regroupement de commandes pour une réutilisation en tant que fonction. 

Pour en savoir plus: [Editing and Executing Code](https://support.rstudio.com/hc/en-us/articles/200484448).

### Gestion de fichiers

On utilisera l'onglet **Files** de RStudio. Celui-ci permet également de créer de nouveaux répertoires (New Folder) et de définir le répertoire courant comme répertoire de travail

    Files -> More -> Set AS Working Directory).

### Historique

RStudio maintient une base de données de toutes les commandes que vous avez déjà entrées dans la console. Vous pouvez parcourir et rechercher dans cette base de données à l'aide de l'onglet **History**, afficher le contexte d'une commande. 
Les commandes sélectionnées dans l'onglet **History** peuvent être utilisées de deux façons (correspondant aux deux boutons sur le côté gauche de la barre d'outils Historique): envoyer la(les) commande(s) sélectionnée(s) à la console, insèrer la(les) commande(s) sélectionnée(s) dans le document source actif

Pour en savoir plus: [Using Command History](https://support.rstudio.com/hc/en-us/articles/200526217-Command-History).


Pour en savoir plus sur RStudio globalement: [Using RStudio IDE](https://support.rstudio.com/hc/en-us/sections/200107586-Using-the-RStudio-IDE)

## Utiliser R 
Il y a plusieurs méthodes pour travailler avec R

* En **mode commande** (CLI) avec un éditeur et une interface graphique ou pas.
* À partir d'un environnement de développement intégré (IDE). L'objectif de cet IDE est d'automatiser et de simplifier les activités du programmeur. Suivant les systèmes d'exploitation, il existe des interfaces de développement différentes.
* En utilisant le module **ESS** (Emacs Speaks Statistics) de l'éditeur **emacs**.

Dans ce cours nous utiliserons l'IDE **RStudio** utilisable sur la plupart des plates-formes (Windows, Linux, MaxOS).
 

## Utiliser R en mode Console
Le langage R est un *langage interprété*. Au démarrage d'une nouvelle session de R, on interagit avec R dans une ** console**.

On écrit une ligne de code dans la ** console**, on la valide (Entrée) et on observe le résultat (pas besoin d'une étape préalable de compilation du code).

### Pour lancer une console R
   - sous Windows, lancer le programme **Rxxx** (xxx correspondant au numéro de  version) dont un raccourci a été créé sur le bureau après l'installation.
 <img src="../figures/Console-R-Windows.jpg",width="60%",height="60%">  
   
   
   - sous Mac OS X, lancer le programme R présent dans le dossier Applications ou la commande **R** dans un terminal suivant le type d'installation de R choisi.
   <img src="../figures/Console-R-Macos.jpg",width="60%",height="60%">
   
   - sous Linux (et plus généralement tout système Unix), **ouvrir un terminal** et **lancer la commande R**.
    <img src="../figures/consoleLinux.jpg",width="60%",height="60%">  
    
   - Au premier lancement de RStudio, l’écran principal est découpé en trois grandes zones :

    <img src="../figures/interfaceRStudio_0.jpg",width="60%",height="60%">
    
**La zone de gauche est la console**. 
    
Les interfaces graphiques sont différentes entre les différents systèmes d'exploitation mais cela ne change rien au langage qui sera compatible avec toutes les plates-formes.

Dans la suite du cours, nous utiliserons la console de **RStudio** ou les notebooks sur le serveur de notebooks de GRICAD.


### Utilisation de R comme une simple calculatrice 

R utilise des fonctions et des opérateurs qui agissent sur des objets:

   - Addition       : **+** 
   - Soustraction   : **-**
   - Multiplication: **\***
   - Division: **/**
   - Exponentiation: **^**  élève le nombre à sa gauche à la puissance du nombre à sa droite, par exemple 3^2 = 9 .
   - Modulo: **%%** reste de la division entière du nombre à sa gauche par le nombre à sa droite, 13%%3 = 1
   - Division entière : **%/%** division entière du nombre à sa gauche par le nombre à sa droite, 13%/%3 = 4

Une liste de *fonctions génériques prédéfinies* (log, sqrt,...)

Pour obtenir ces informations complètes, utiliser la commande **help("Math")**
    
    dans la console de RStudio, on essaie et on observe...


```R
# R comme calculatrice
2+3
3-2
2^3
13 %% 3
13 %/% 3
log(7) #fonction prédéfinie de R
runif(4)
"espace de travail"
ls() # fournit le nom des objets de l'espace de travail
```


5



1



8



1



4



1.94591014905531



<ol class=list-inline>
	<li>0.716547878459096</li>
	<li>0.827923827804625</li>
	<li>0.607921322109178</li>
	<li>0.211772634647787</li>
</ol>




'espace de travail'






## Programmation sous R - Espace de travail

Un concept de base en programmation s'appelle une **variable**.

Une variable vous permet de **stocker une valeur** (par exemple 4) ou **un objet** (par exemple une description de fonction)  à l’aide de l’*opérateur d’assignation* 

   opérateur d'assignation ** " <- "** dans R. 

Vous pouvez ensuite utiliser le nom de cette variable pour accéder facilement à la valeur ou à l'objet stocké dans cette variable.

On peut retrouver l'ensemble des objets stockés avec la fonction **ls()** dans la console ou dans l'onglet **Environment** de RStudio.

On essaie et on observe...



```R
# R comme langage interprete - espace de travail
ls()
n <- 5
n
a <- runif(n)
a
b <- 10
nb <- n + b
nb
"Espace de travail 1"
ls()
rm(n)
# Espace de travail
"Espace de travail 2"
ls()
# Nettoyer l'espace de travail
rm(list=ls())
"Espace de travail 3"
ls()
```






5



<ol class=list-inline>
	<li>0.62501686764881</li>
	<li>0.938050465425476</li>
	<li>0.316431388491765</li>
	<li>0.902880884939805</li>
	<li>0.355515625793487</li>
</ol>




15



'Espace de travail 1'



<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'n'</li>
	<li>'nb'</li>
</ol>




'Espace de travail 2'



<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'nb'</li>
</ol>




'Espace de travail 3'






### Quelques statistiques


```R
ls()
a <- 4
a
```






4



```R
# R comme langage interprete - statistiques
n <-20
a <- runif(n)
mean(a)
sd(a)
summary(a)
```


0.447511035855859



0.267335138241474



        Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
    0.001341 0.220600 0.514800 0.447500 0.608800 0.840900 



```R
barplot(a)
```


![png](output_17_0.png)



```R
b <- rnorm(20,mean=0,sd=1)
hist(b)
```


![png](output_18_0.png)



```R
# Les distributions du package de base
help(Distributions)
```

### Espace de travail (workspace)

L'espace de travail est l'ensemble des **objets** stockés, consultable:

* Dans la console par la fonction ** ls() **.

* Dans l'onglet **"Environment"** de RStudio.


```R
# Espace de travail (workspace)
"Espace de travail 1"
ls()
a <- 7
"Espace de travail 2"
ls()
#
myfunc <- function() {y <- 1; ls()}
myfunc() 
# Effacer des objets 
rm(a)
"Espace de travail 3"
ls()
rm(myfunc)
"Espace de travail 4"
ls()
#
cmat1 <-matrix(1:20,ncol=5)
cmat2 <-matrix(1:20,ncol=4)
"Espace de travail 5"
ls()
# Effacer tout l'espace de travail
rm(list = ls())
"Espace de travail 6"
ls()
```


'Espace de travail 1'



<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'n'</li>
</ol>




'Espace de travail 2'



<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'n'</li>
</ol>




'y'



'Espace de travail 3'



<ol class=list-inline>
	<li>'b'</li>
	<li>'myfunc'</li>
	<li>'n'</li>
</ol>




'Espace de travail 4'



<ol class=list-inline>
	<li>'b'</li>
	<li>'n'</li>
</ol>




'Espace de travail 5'



<ol class=list-inline>
	<li>'b'</li>
	<li>'cmat1'</li>
	<li>'cmat2'</li>
	<li>'n'</li>
</ol>




'Espace de travail 6'







Pour en savoir plus: [Working Directories and Workspaces](https://support.rstudio.com/hc/en-us/articles/200711843-Working-Directories-and-Workspaces).

### Les types de base en R

R utilise différents types de données dont les principaux sont:

   * Un objet vide noté: NULL
   * Les *valeurs réelles* comme 4.5 sont appelées **numériques**.
   * Les *nombres naturels* comme 4 sont appelés *integer*. Les nombres entiers sont aussi **numériques**.
   * Les *valeurs booléennes* (TRUE ou FALSE) sont appelées **logiques**.
   * Les *chaînes de caractères* comme "nom" sont appelées **character**.
 
Les guillemets de part et d'autre d'un texte ( "texte") indique que c'est du type **character**

Pour connaître le mode d'un objet **x** de R, il suffit d'exécuter la fonction **mode(x)**

* La fonction **typeof()** permet d'obtenir une description plus précise de la représentation interne d'un objet.

* R utilise un mécanisme de fonction générique simple, le choix de la méthode utilisée dépend de la classe du premier argument de la fonction générique (ex: plot, print,...).

Pour connaître la classe d'une variable, on utilise la fonction **class()**


```R
# Variables of differents types
my_numeric <- 426
my_character <- "42"
my_logical <- FALSE 
my_integer <- 5L
```


```R
# Class de my_numeric
" mode, typeof et classe my_numeric"
mode(my_numeric)
typeof(my_numeric)
class(my_numeric)
```


' mode, typeof et classe my_numeric'



'numeric'



'double'



'numeric'



```R
# Class de my_numeric
" mode, typeof et classe my_integer"
mode(my_integer)
typeof(my_integer)
class(my_integer)

xx <- my_numeric + my_integer
xx
mode(xx)
class(xx)
```


' mode, typeof et classe my_integer'



'numeric'



'integer'



'integer'



431



'numeric'



'numeric'



```R
# Class de my_character
" mode, typeof et classe my_character"
mode(my_character)
typeof(my_character)
class(my_character)
```


' mode, typeof et classe my_character'



'character'



'character'



'character'



```R
# my_character + 10  # argument non numérique pour un opérateur binaire
paste(my_character, "euros") # Concatène après conversion en caractères
b <- paste(my_character, 14)
b
mode(b)
class(b)
```


'42 euros'



'42 14'



'character'



'character'



```R
# Class de my_logical
" mode et classe my_logical"
mode(my_logical)
class(my_logical)
```


' mode et classe my_logical'



'logical'



'logical'


#### Tester/ concertir le mode d'un objet

* Il est possible de tester le **mode** d'un objet **x** de R avec les fonctions:
   - **is.null**(x)
   - **is.numeric**(x)
   - **is.logical**(x)
   - **is.character**(x)
   - **is.complex**(x)
   
Le résultat de ces fonctions est un booléen qui prend les valeurs **TRUE** et **FALSE**

* Il est possible de convertir un objet d'un mode à un autre avec les fonctions:
   - **as.numeric**(x)
   - **as.logical**(x)
   - **as.character**(x)
   - **as.complex**(x)
   
Il faut rester prudent quant à la signification de ces conversions, R retourne toujours un résultat même si la conversion n'a pas de sens...


```R
# Conversion "logical" en "numeric"
a <- TRUE
mode(a)
d <- as.numeric(a)
mode(d)

# Conversion "logical" en "character"
as.character(a)

# Conversion "character" en "numeric"
b <- "3"
as.numeric(b)
6 + as.numeric(b)

```


'logical'



'numeric'



'TRUE'



3



9




#### Exemple de fonction générique 

La fonction **print**


```R
# fonction generique
a <- 1:20
print(a)

b <- 5.8
class(b)
print(b)

# objet de classe "matrix"
cmat1 <-matrix(a,ncol=5)
class(cmat1)
print(cmat1)

cmat2 <-matrix(a,ncol=4)
class(cmat2)
print(cmat2)

```

     [1]  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20



'numeric'


    [1] 5.8



'matrix'


         [,1] [,2] [,3] [,4] [,5]
    [1,]    1    5    9   13   17
    [2,]    2    6   10   14   18
    [3,]    3    7   11   15   19
    [4,]    4    8   12   16   20



'matrix'


         [,1] [,2] [,3] [,4]
    [1,]    1    6   11   16
    [2,]    2    7   12   17
    [3,]    3    8   13   18
    [4,]    4    9   14   19
    [5,]    5   10   15   20


#### Valeur manquante et valeurs spéciales

* Certaines données sont **absentes** ou **inexploitables**, elles ne sont pas disponibles pour le traitement, R les note **NA** pour **Not Available**.

Pour savoir où se trouvent les données manquantes, on utilise la fonction **is.na**(x) qui retourne un booléen de même longueur que x.

* On peut mentionner également  les valeurs spéciales **Inf** pour *l'infini* et **NaN** pour *Not a Number*.

et les fonctions de test associés:
    
       - is.finite(x)
       - is.infinite(x)
       - is.nan(x)


```R
# donnees manquantes
v <- c(2,6,4,8,2,9,10)
v
mean(v)

vv <- c(2,6,4,8,2,NA,10)
mean(vv)
mean(vv,na.rm=TRUE)
```


<ol class=list-inline>
	<li>2</li>
	<li>6</li>
	<li>4</li>
	<li>8</li>
	<li>2</li>
	<li>9</li>
	<li>10</li>
</ol>




5.85714285714286



    [1] NA



5.33333333333333



```R
# Valeurs spéciales
exp(1e10)
w <- c(log(-4),6,9)
w
mean(w)
mean(w,na.rm=TRUE)
```


Inf


    Warning message in log(-4):
    “NaNs produced”


<ol class=list-inline>
	<li>NaN</li>
	<li>6</li>
	<li>9</li>
</ol>




NaN



7.5


## Utilisation de scripts

Il n’est pas toujours très pratique de travailler uniquement sur la console, la saisie est réalisée ligne à ligne. En cas d’erreur de saisie, on devra soit resaisir la commande en la corrigeant, soit la rappeler en la recherchant dans l’historique des commandes en utilisant l'onglet **History** de RStudio pour pouvoir la modifier .

La méthode utilisée est de construire des scripts. Un script est un fichier texte qui contient une séquence d'instructions. La saisie de ces instructions peut se faire à l'extérieur de RStudio avec un éditeur (nedit, emacs, notepad,...) ou directement dans l'éditeur de RStudio.

Pour créer un nouveau script, utiliser le menu:

    File -> New File -> R Script

Une fenêtre d'édition s'ouvre dans laquelle vous pouvez saisir vos instructions, exécuter instruction par instruction ou globalement, sauvegarder le script, charger des scripts existants...

Pour en savoir plus: [Editing and Executing Code](https://support.rstudio.com/hc/en-us/articles/200484448-Editing-and-Executing-Code)


## Qu'est ce qu'un Data Frame?

**Un tableau de données est un ensemble de vecteurs rangés colonne par colonne**. Chaque colonne du tableau data correspond à une variable, chaque ligne à un individu. Les variables peuvent être de type différent (numérique, booléen, character,...).

C'est ce même concept qu'on utilise dans les progiciels statistiques tels que SAS, BMDP, SPSS...

Sous R, les tableaux de données sont des objets particuliers appelés **data.frame**. On verra dans la suite du cours que tout type d'objet a deux attributs intrinsèques, **son mode** et **sa longueur**. Il peut
avoir des attributs spécifiques qui différents selon le type de l'objet (dim,dimnames,class,...).

Pour obtenir les attributs d'un objet, on utilise la fonction **attributes()**. Les attributs d'un **data-frame** sont le nom des variables (*names*), le nom des individus (*row.names*), les dimensions du tableau, le nombre des lignes et des colonnes.

Pour accéder à la valeur de ces attributs, on utile le caractère **\$** et le nom de l'attribut.

La fonction **dim()** fournit les dimensions du tableau de données: le nombre d'observations et le nombre de variables.

La fonction **dimnames()** fournit le nom des dimensions du tableau de données: le nom des individus et le nom des variables.


### Traitement du fichier de données "mtcars.csv"

* *Description* : les données ont été extraites du magazine Motor Trend US de 1974 et comprennent la consommation de carburant et 10 aspects de la conception et des performances de 32 automobiles (modèles 1973-1974).

* *Format*: 

Un **fichier de données** "mtcars" avec 32 *observations* sur 11 *variables*:

    mpg: Miles/(US) gallon (variable quantitative)
    cyl: Number of cylinders (variable qualitative)
    disp: Displacement (cu.in.) (variable quantitative)
    hp:	Gross horsepower (variable quantitative)
    drat: Rear axle ratio (variable quantitative)
    wt:	Weight (1000 lbs) (variable quantitative)
    qsec: 1/4 mile time (variable quantitative)
    vs: V/S (variable qualitative)
    am:	Transmission (0 = automatic, 1 = manual) (variable qualitative)
    gear: Number of forward gears (variable qualitative)
    carb: Number of carburetors (variable qualitative)
    
La première ligne du fichier de données (mtcars.csv) contient les labels des 11 variables et pour tout individu, l'observation de chaque variable, séparée par **","**.

<img src="../figures/donMtcars.jpg",width="60%",height="60%">

On lit le fichier de données avec la fonction **read.csv()** adaptée au format qui fournit en sortie un objet de type **data-frame**. D'autres fonctions sont disponibles pour d'autres formats de données.

Le fichier de données se trouve dans le répertoire **data**.


```R
help(read.csv)
```


```R
ls() # Contenu de l'espace de travail
```


<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'cmat1'</li>
	<li>'cmat2'</li>
	<li>'d'</li>
	<li>'my_character'</li>
	<li>'my_integer'</li>
	<li>'my_logical'</li>
	<li>'my_numeric'</li>
	<li>'v'</li>
	<li>'vv'</li>
	<li>'w'</li>
	<li>'xx'</li>
</ol>




```R
# Lecture du fichier de données
mtcars <- read.csv("data/mtcars.csv",header=TRUE,sep=",")
ls()
```


<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'cmat1'</li>
	<li>'cmat2'</li>
	<li>'d'</li>
	<li>'mtcars'</li>
	<li>'my_character'</li>
	<li>'my_integer'</li>
	<li>'my_logical'</li>
	<li>'my_numeric'</li>
	<li>'v'</li>
	<li>'vv'</li>
	<li>'w'</li>
	<li>'xx'</li>
</ol>




```R
class(mtcars)
attributes(mtcars)
mtcars$names  # attribut qui contient le nom des variables
```


'data.frame'



<dl>
	<dt>$names</dt>
		<dd><ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>
</dd>
	<dt>$class</dt>
		<dd>'data.frame'</dd>
	<dt>$row.names</dt>
		<dd><ol class=list-inline>
	<li>1</li>
	<li>2</li>
	<li>3</li>
	<li>4</li>
	<li>5</li>
	<li>6</li>
	<li>7</li>
	<li>8</li>
	<li>9</li>
	<li>10</li>
	<li>11</li>
	<li>12</li>
	<li>13</li>
	<li>14</li>
	<li>15</li>
	<li>16</li>
	<li>17</li>
	<li>18</li>
	<li>19</li>
	<li>20</li>
	<li>21</li>
	<li>22</li>
	<li>23</li>
	<li>24</li>
	<li>25</li>
	<li>26</li>
	<li>27</li>
	<li>28</li>
	<li>29</li>
	<li>30</li>
	<li>31</li>
	<li>32</li>
</ol>
</dd>
</dl>




    NULL



```R
mtcars
```


<table>
<thead><tr><th scope=col>mpg</th><th scope=col>cyl</th><th scope=col>disp</th><th scope=col>hp</th><th scope=col>drat</th><th scope=col>wt</th><th scope=col>qsec</th><th scope=col>vs</th><th scope=col>am</th><th scope=col>gear</th><th scope=col>carb</th></tr></thead>
<tbody>
	<tr><td>21.0 </td><td>6    </td><td>160.0</td><td>110  </td><td>3.90 </td><td>2.620</td><td>16.46</td><td>0    </td><td>1    </td><td>4    </td><td>4    </td></tr>
	<tr><td>21.0 </td><td>6    </td><td>160.0</td><td>110  </td><td>3.90 </td><td>2.875</td><td>17.02</td><td>0    </td><td>1    </td><td>4    </td><td>4    </td></tr>
	<tr><td>22.8 </td><td>4    </td><td>108.0</td><td> 93  </td><td>3.85 </td><td>2.320</td><td>18.61</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>21.4 </td><td>6    </td><td>258.0</td><td>110  </td><td>3.08 </td><td>3.215</td><td>19.44</td><td>1    </td><td>0    </td><td>3    </td><td>1    </td></tr>
	<tr><td>18.7 </td><td>8    </td><td>360.0</td><td>175  </td><td>3.15 </td><td>3.440</td><td>17.02</td><td>0    </td><td>0    </td><td>3    </td><td>2    </td></tr>
	<tr><td>18.1 </td><td>6    </td><td>225.0</td><td>105  </td><td>2.76 </td><td>3.460</td><td>20.22</td><td>1    </td><td>0    </td><td>3    </td><td>1    </td></tr>
	<tr><td>14.3 </td><td>8    </td><td>360.0</td><td>245  </td><td>3.21 </td><td>3.570</td><td>15.84</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>24.4 </td><td>4    </td><td>146.7</td><td> 62  </td><td>3.69 </td><td>3.190</td><td>20.00</td><td>1    </td><td>0    </td><td>4    </td><td>2    </td></tr>
	<tr><td>22.8 </td><td>4    </td><td>140.8</td><td> 95  </td><td>3.92 </td><td>3.150</td><td>22.90</td><td>1    </td><td>0    </td><td>4    </td><td>2    </td></tr>
	<tr><td>19.2 </td><td>6    </td><td>167.6</td><td>123  </td><td>3.92 </td><td>3.440</td><td>18.30</td><td>1    </td><td>0    </td><td>4    </td><td>4    </td></tr>
	<tr><td>17.8 </td><td>6    </td><td>167.6</td><td>123  </td><td>3.92 </td><td>3.440</td><td>18.90</td><td>1    </td><td>0    </td><td>4    </td><td>4    </td></tr>
	<tr><td>16.4 </td><td>8    </td><td>275.8</td><td>180  </td><td>3.07 </td><td>4.070</td><td>17.40</td><td>0    </td><td>0    </td><td>3    </td><td>3    </td></tr>
	<tr><td>17.3 </td><td>8    </td><td>275.8</td><td>180  </td><td>3.07 </td><td>3.730</td><td>17.60</td><td>0    </td><td>0    </td><td>3    </td><td>3    </td></tr>
	<tr><td>15.2 </td><td>8    </td><td>275.8</td><td>180  </td><td>3.07 </td><td>3.780</td><td>18.00</td><td>0    </td><td>0    </td><td>3    </td><td>3    </td></tr>
	<tr><td>10.4 </td><td>8    </td><td>472.0</td><td>205  </td><td>2.93 </td><td>5.250</td><td>17.98</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>10.4 </td><td>8    </td><td>460.0</td><td>215  </td><td>3.00 </td><td>5.424</td><td>17.82</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>14.7 </td><td>8    </td><td>440.0</td><td>230  </td><td>3.23 </td><td>5.345</td><td>17.42</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>32.4 </td><td>4    </td><td> 78.7</td><td> 66  </td><td>4.08 </td><td>2.200</td><td>19.47</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>30.4 </td><td>4    </td><td> 75.7</td><td> 52  </td><td>4.93 </td><td>1.615</td><td>18.52</td><td>1    </td><td>1    </td><td>4    </td><td>2    </td></tr>
	<tr><td>33.9 </td><td>4    </td><td> 71.1</td><td> 65  </td><td>4.22 </td><td>1.835</td><td>19.90</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>21.5 </td><td>4    </td><td>120.1</td><td> 97  </td><td>3.70 </td><td>2.465</td><td>20.01</td><td>1    </td><td>0    </td><td>3    </td><td>1    </td></tr>
	<tr><td>15.5 </td><td>8    </td><td>318.0</td><td>150  </td><td>2.76 </td><td>3.520</td><td>16.87</td><td>0    </td><td>0    </td><td>3    </td><td>2    </td></tr>
	<tr><td>15.2 </td><td>8    </td><td>304.0</td><td>150  </td><td>3.15 </td><td>3.435</td><td>17.30</td><td>0    </td><td>0    </td><td>3    </td><td>2    </td></tr>
	<tr><td>13.3 </td><td>8    </td><td>350.0</td><td>245  </td><td>3.73 </td><td>3.840</td><td>15.41</td><td>0    </td><td>0    </td><td>3    </td><td>4    </td></tr>
	<tr><td>19.2 </td><td>8    </td><td>400.0</td><td>175  </td><td>3.08 </td><td>3.845</td><td>17.05</td><td>0    </td><td>0    </td><td>3    </td><td>2    </td></tr>
	<tr><td>27.3 </td><td>4    </td><td> 79.0</td><td> 66  </td><td>4.08 </td><td>1.935</td><td>18.90</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>26.0 </td><td>4    </td><td>120.3</td><td> 91  </td><td>4.43 </td><td>2.140</td><td>16.70</td><td>0    </td><td>1    </td><td>5    </td><td>2    </td></tr>
	<tr><td>30.4 </td><td>4    </td><td> 95.1</td><td>113  </td><td>3.77 </td><td>1.513</td><td>16.90</td><td>1    </td><td>1    </td><td>5    </td><td>2    </td></tr>
	<tr><td>15.8 </td><td>8    </td><td>351.0</td><td>264  </td><td>4.22 </td><td>3.170</td><td>14.50</td><td>0    </td><td>1    </td><td>5    </td><td>4    </td></tr>
	<tr><td>19.7 </td><td>6    </td><td>145.0</td><td>175  </td><td>3.62 </td><td>2.770</td><td>15.50</td><td>0    </td><td>1    </td><td>5    </td><td>6    </td></tr>
	<tr><td>15.0 </td><td>8    </td><td>301.0</td><td>335  </td><td>3.54 </td><td>3.570</td><td>14.60</td><td>0    </td><td>1    </td><td>5    </td><td>8    </td></tr>
	<tr><td>21.4 </td><td>4    </td><td>121.0</td><td>109  </td><td>4.11 </td><td>2.780</td><td>18.60</td><td>1    </td><td>1    </td><td>4    </td><td>2    </td></tr>
</tbody>
</table>




```R
mtcars[,"disp"] # variable disp dans le data-frame "mtcars"
mtcars[["disp"]]
```


<ol class=list-inline>
	<li>160</li>
	<li>160</li>
	<li>108</li>
	<li>258</li>
	<li>360</li>
	<li>225</li>
	<li>360</li>
	<li>146.7</li>
	<li>140.8</li>
	<li>167.6</li>
	<li>167.6</li>
	<li>275.8</li>
	<li>275.8</li>
	<li>275.8</li>
	<li>472</li>
	<li>460</li>
	<li>440</li>
	<li>78.7</li>
	<li>75.7</li>
	<li>71.1</li>
	<li>120.1</li>
	<li>318</li>
	<li>304</li>
	<li>350</li>
	<li>400</li>
	<li>79</li>
	<li>120.3</li>
	<li>95.1</li>
	<li>351</li>
	<li>145</li>
	<li>301</li>
	<li>121</li>
</ol>




<ol class=list-inline>
	<li>160</li>
	<li>160</li>
	<li>108</li>
	<li>258</li>
	<li>360</li>
	<li>225</li>
	<li>360</li>
	<li>146.7</li>
	<li>140.8</li>
	<li>167.6</li>
	<li>167.6</li>
	<li>275.8</li>
	<li>275.8</li>
	<li>275.8</li>
	<li>472</li>
	<li>460</li>
	<li>440</li>
	<li>78.7</li>
	<li>75.7</li>
	<li>71.1</li>
	<li>120.1</li>
	<li>318</li>
	<li>304</li>
	<li>350</li>
	<li>400</li>
	<li>79</li>
	<li>120.3</li>
	<li>95.1</li>
	<li>351</li>
	<li>145</li>
	<li>301</li>
	<li>121</li>
</ol>




```R
mtcars[1:4,] # Observations de toutes les variable sur les 4 premiers individus
```


<table>
<thead><tr><th scope=col>mpg</th><th scope=col>cyl</th><th scope=col>disp</th><th scope=col>hp</th><th scope=col>drat</th><th scope=col>wt</th><th scope=col>qsec</th><th scope=col>vs</th><th scope=col>am</th><th scope=col>gear</th><th scope=col>carb</th></tr></thead>
<tbody>
	<tr><td>21.0 </td><td>6    </td><td>160  </td><td>110  </td><td>3.90 </td><td>2.620</td><td>16.46</td><td>0    </td><td>1    </td><td>4    </td><td>4    </td></tr>
	<tr><td>21.0 </td><td>6    </td><td>160  </td><td>110  </td><td>3.90 </td><td>2.875</td><td>17.02</td><td>0    </td><td>1    </td><td>4    </td><td>4    </td></tr>
	<tr><td>22.8 </td><td>4    </td><td>108  </td><td> 93  </td><td>3.85 </td><td>2.320</td><td>18.61</td><td>1    </td><td>1    </td><td>4    </td><td>1    </td></tr>
	<tr><td>21.4 </td><td>6    </td><td>258  </td><td>110  </td><td>3.08 </td><td>3.215</td><td>19.44</td><td>1    </td><td>0    </td><td>3    </td><td>1    </td></tr>
</tbody>
</table>




```R
names(mtcars)
row.names(mtcars)
```


<ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>




<ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
	<li>'6'</li>
	<li>'7'</li>
	<li>'8'</li>
	<li>'9'</li>
	<li>'10'</li>
	<li>'11'</li>
	<li>'12'</li>
	<li>'13'</li>
	<li>'14'</li>
	<li>'15'</li>
	<li>'16'</li>
	<li>'17'</li>
	<li>'18'</li>
	<li>'19'</li>
	<li>'20'</li>
	<li>'21'</li>
	<li>'22'</li>
	<li>'23'</li>
	<li>'24'</li>
	<li>'25'</li>
	<li>'26'</li>
	<li>'27'</li>
	<li>'28'</li>
	<li>'29'</li>
	<li>'30'</li>
	<li>'31'</li>
	<li>'32'</li>
</ol>




```R
dimnames(mtcars)
```


<ol>
	<li><ol class=list-inline>
	<li>'1'</li>
	<li>'2'</li>
	<li>'3'</li>
	<li>'4'</li>
	<li>'5'</li>
	<li>'6'</li>
	<li>'7'</li>
	<li>'8'</li>
	<li>'9'</li>
	<li>'10'</li>
	<li>'11'</li>
	<li>'12'</li>
	<li>'13'</li>
	<li>'14'</li>
	<li>'15'</li>
	<li>'16'</li>
	<li>'17'</li>
	<li>'18'</li>
	<li>'19'</li>
	<li>'20'</li>
	<li>'21'</li>
	<li>'22'</li>
	<li>'23'</li>
	<li>'24'</li>
	<li>'25'</li>
	<li>'26'</li>
	<li>'27'</li>
	<li>'28'</li>
	<li>'29'</li>
	<li>'30'</li>
	<li>'31'</li>
	<li>'32'</li>
</ol>
</li>
	<li><ol class=list-inline>
	<li>'mpg'</li>
	<li>'cyl'</li>
	<li>'disp'</li>
	<li>'hp'</li>
	<li>'drat'</li>
	<li>'wt'</li>
	<li>'qsec'</li>
	<li>'vs'</li>
	<li>'am'</li>
	<li>'gear'</li>
	<li>'carb'</li>
</ol>
</li>
</ol>




```R
summary(mtcars)
```


          mpg             cyl             disp             hp       
     Min.   :10.40   Min.   :4.000   Min.   : 71.1   Min.   : 52.0  
     1st Qu.:15.43   1st Qu.:4.000   1st Qu.:120.8   1st Qu.: 96.5  
     Median :19.20   Median :6.000   Median :196.3   Median :123.0  
     Mean   :20.09   Mean   :6.188   Mean   :230.7   Mean   :146.7  
     3rd Qu.:22.80   3rd Qu.:8.000   3rd Qu.:326.0   3rd Qu.:180.0  
     Max.   :33.90   Max.   :8.000   Max.   :472.0   Max.   :335.0  
          drat             wt             qsec             vs        
     Min.   :2.760   Min.   :1.513   Min.   :14.50   Min.   :0.0000  
     1st Qu.:3.080   1st Qu.:2.581   1st Qu.:16.89   1st Qu.:0.0000  
     Median :3.695   Median :3.325   Median :17.71   Median :0.0000  
     Mean   :3.597   Mean   :3.217   Mean   :17.85   Mean   :0.4375  
     3rd Qu.:3.920   3rd Qu.:3.610   3rd Qu.:18.90   3rd Qu.:1.0000  
     Max.   :4.930   Max.   :5.424   Max.   :22.90   Max.   :1.0000  
           am              gear            carb      
     Min.   :0.0000   Min.   :3.000   Min.   :1.000  
     1st Qu.:0.0000   1st Qu.:3.000   1st Qu.:2.000  
     Median :0.0000   Median :4.000   Median :2.000  
     Mean   :0.4062   Mean   :3.688   Mean   :2.812  
     3rd Qu.:1.0000   3rd Qu.:4.000   3rd Qu.:4.000  
     Max.   :1.0000   Max.   :5.000   Max.   :8.000  



```R
ls()
```





### Sauvegarde

Il est possible de sauvegarder des données et/ou des résultats au format **RData**, format propre au logiciel R. Cela facilite l’archivage de résultats intermédiaires ou l’enregistrement d’un tableau de données nettoyé (recodage de 
valeurs manquantes, modification des modalités de variables qualitatives, correction des erreurs de saisie,...) ou augmenté de variables auxiliaires. Pour cela, on utilise la commande **save()**. La commande **load()** permet de recharger des données sauvegardées au format **RData**.


```R
# Contenu de l'espace de travail
ls()
save(mtcars,file="data/mtcars.RData")
```


<ol class=list-inline>
	<li>'a'</li>
	<li>'b'</li>
	<li>'cmat1'</li>
	<li>'cmat2'</li>
	<li>'d'</li>
	<li>'mtcars'</li>
	<li>'my_character'</li>
	<li>'my_integer'</li>
	<li>'my_logical'</li>
	<li>'my_numeric'</li>
	<li>'v'</li>
	<li>'vv'</li>
	<li>'w'</li>
	<li>'xx'</li>
</ol>



** Load**


```R
rm(list=ls())
"espace de travail 1"
ls()
load("data/mtcars.RData")
"espace de travail 2"
ls()
```


'espace de travail 1'







'espace de travail 2'



'mtcars'


## Quitter R

Pour quitter **R**, exécuter la fonction 

   **q()**   # R vous demande
   
   **Save workspace image?** [y/n/c]: 

Si vous répondez "**y**" , vous sauvegardez la session, R crée deux  fichiers dans le répertoire de travail, **.Rhistory** et **.RData.**

   * **.Rhistory**:  historique des commandes de la session
   * **..RData**:  environnement (workspace) de la session.
    
  Attention !!! Nécessité de gérer le volume de ces sauvegardes...
  
Lorsque vous réouvrez une session, vous retrouvez l'environnement et l'historique de la session sauvegardée.

## Bibliothèques ou package

Un package ou une bibliothèque de programmes externes, est un ensemble de programme R, qui complète les fonctionnalités de R. 

* Liste des package installés


```R
library()
mat <- available.packages() # Packages disponibles (au CRAN)
```



* Charger un package(si la librairie est déjà chargée, ne fait rien)


```R
library(grDevices)
sessionInfo()     # packages chargés en cours de session
```

* Liste des fonctions d'un package


```R
library(help="stats")  #fonctions du package "stats"
```

* Liste des datasets disponibles comme exemples sous R


```R
data()
data(package="datasets") # Datasets pour le package datasets
```

### Installer package

Installation d'un package en tant qu'administrateur

               install.packages("gplots") # sous root

### Mise à jour d'un package : 
Certains packages sont en constante évolution avec de nouvelles versions disponibles. Il est préférable de les mettre à jour de temps en temps.

                 update.packages()

**Attention, la mise à jour d'un package est fonction de la version de R**. Ilfaudra mettre à jour R régulièrement. 

### Créer ses propres packages
L'utilisateur peut créer ses propres packages(R avancé), les difuser sur le [CRAN](https://cran.r-project.org) ou pour l'usage de sa propre communuaté.

### Installer un package en local

Utilisation de la variable d'environnement **RLIBS** qui fournit le/les répertoire(s) d'installation en local des packages

            export RLIBS="/home/yourusername/R/lib
            install.packages("gplots") # en local dans R_LIBS
            


```R
.libPaths()  # liste des repertoires de recherche des librairies
```


'/opt/conda/lib/R/library'


Si vous ne voulez pas mettre à jour la variable à chaque installation.

* Placer la commande *export* dans le fichier **.bashrc** de votre "home directory", elle sera exécutée à chaque session.

ou, 

* dans un fichier **.Renviron** de votre "home directory", la ligne

     *RLIBS="/home/yourusername/R/lib"*

Vous pouvez indiquez plusieurs répertoires, séparés par **":"** 

* Exécuter la fonction **.libPaths()** et observez. 


```R
.libPaths()  # liste des repertoires de recherche des librairies
```


'/opt/conda/lib/R/library'


* Ajouter un répertoire local

   *.libPaths("<path du repertoire local>")*
            
Plusieurs répertoires possibles, séparés par ":".


```R
.libPaths("/home/viryl/R/lib") # ajout d'un repertoire d'installation local
```


```R
.libPaths()
```


<ol class=list-inline>
	<li>'/home/viryl/R/lib'</li>
	<li>'/opt/conda/lib/R/library'</li>
</ol>



## Documentation 

1. Dans la console:
    - La fonction **help(ma_function)** ou **?ma_fonction** dans la console affiche l'aide dans l'onglet **Help** de l'interfcae RStudio. On y trouve:
        * la description de la fonction,
        * les arguments optionnels ou obligatoires utilisés dans cette fonction, leur signification,
        * les sorties de la fonction,
        * les références,
        * quelques exemples
    


```R
help(plot) # ou ?plot donne le même résultat. 
```

   - ** Sur les détails de programmation ** 
           


```R
help("[[") # encadré d'une simple ou double quote
```


```R
help("if"); help("for")
```

   - La fonction **help.start()** permet d'ouvrir la version online (HTML) de l'aide R.
   - Si on ne connait pas exactement le nom d'une fonction que l'on souhaite utiliser, il est alors possible de retrouver cette fonction grâce à **help.search("mot_clé")** qui vous affichera une liste des fonctions en rapport avec le mot_clé.
   - Si on ne se souvient que d'une partie du nom d'une fonction, on peut alors utiliser la fonction **apropos("pattern")** pour lister les noms des fonctions qui contiennent ce pattern.
   - Si aucune de ces fonctions n'a permis de résoudre votre problème, il reste la fonction **RSiteSearch("mot_clé1 mot_clé2 ...")** qui permet de faire directement une recherche dans la "R-help mailing list" (sorte de forum de discussion dédié à l'utilisation de R) ainsi que dans toutes les documentations de R et affiche les résultats dans une page Web.
   
2. On accède à une aide complète sur l'utilisation, l'installation de R dans l'onglet **Help** de **RStudio**.

3. De nombreux tutoriaux sur internet.


```R
help.search("correlation ")
```


```R
apropos("NA")
```

### Bibliographie

#### Sites Web

* [Site RStudio](https://www.rstudio.com/online-learning)
* [Using RStudio IDE](https://support.rstudio.com/hc/en-us/sections/200107586-Using-the-RStudio-IDE)
* [Introduction à R (MOOC - Introduction à la statistique avec R](https://www.fun-mooc.fr/c4x/UPSUD/42001S03/asset/introR.html)
* [Introduction à R et au tidyverse](https://juba.github.io/tidyverse/index.html)
* [Cours complet sur l'analyse de données (MOOC)](http://factominer.free.fr/course/MOOC_fr.html)
* [Data Science](https://www.rstudio.com/online-learning/#DataScience)

#### Livres

* [Hands-On Programming with R - Write Your Own Functions and Simulations (Garrett Grolemund)](http://shop.oreilly.com/product/0636920028574.do#)
* [Training books](https://www.rstudio.com/resources/training/books/)


